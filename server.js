'use strict';
var fs = require('fs');
var path = require('path');
var URL = require('url');
var mkdirp = require('mkdirp');
var express = require('express');
var phantom = require('phantom');
var device = require('express-device');
var history = require('connect-history-api-fallback');
var favicon = require('serve-favicon');
var compression = require('compression');
var app = module.exports = express();

var dist_path = '/dist/';
var options = {
  //maxAge: '60d',
  setHeaders: function(res, path, stat) {
    // Webfonts need to have CORS * set in order to work.
    if (path.match(/ttf|woff|woff2|eot|svg/ig)) {
      res.set('Access-Control-Allow-Origin', '*');
    }
  }
};

var count         = 0,
    resourceWait  = 200,
    maxRenderWait = 5000,
    forcedRenderTimeout,
    renderTimeout;

app.use(history());
app.use(compression());
app.use(device.capture());
app.use(favicon(path.join(__dirname, 'dist', 'assets', 'favicon.ico')));
app.use('/assets', express.static(path.join(__dirname, dist_path, 'assets'), options));

app.use(function(req, res) {
  console.log(req.device.type, req.originalUrl);

  if (req.device.type == 'bot' || req.query.hasOwnProperty('_escaped_fragment_')) {//if the user requested a pre-renderred page
    var parsed_url = parseUrl(req.originalUrl);

    var file_name = parsed_url == '/' || parsed_url == '' ? 'index' : parsed_url;
    var file_path = path.join(__dirname, 'dist', 'cache', file_name + '.html');

    if (fs.existsSync(file_path)) {// if the page is renderred, return its content and then cache it again

      res.sendFile(file_path);
      prerender(req, res, file_path, false);
      console.log('from cache');

    } else {// if the page hasn't been cached before, send the content and then cache

      prerender(req, res, file_path, true);
      console.log('from prerender');

    }
  } else {
    //if normal user is requesting the page, send the client project
    res.sendFile(path.join(__dirname, dist_path, 'index.html'));
  }
});

var phInstance;
function prerender (req, res, file_path, sendContent) {
  var parsed_url = parseUrl(req.originalUrl);
  var url = absoluteUrl(req, decodeURIComponent(parsed_url));

  console.info('prerendering url : ' + url);
  //initiate phantom
  phantom.create().then(function(ph) {
    phInstance = ph;

    //create server page
    ph.createPage().then(function(page) {
      console.log("start time", Date());//to track processing time

      //check for the resources that are going to be loaded and wait for them
      page.on('onResourceRequested', function (request, networkRequest) {
        count += 1;
        console.log('resource requested', '> req.id: ' + request.id + ' - req.url: ' + request.url);
        clearTimeout(renderTimeout);
      });

      //check if the resources are loaded and execute the prerenderred page if there is no resources to wait for
      page.on('onResourceReceived', function (response) {
        if (!response.stage || response.stage === 'end') {
          count -= 1;
          console.log('resource received', response.id + ' ' + response.status + ' - ' + response.url);
          if (count === 0) {
            renderTimeout = setTimeout(function () { renderAndSend(page, ph, res, file_path, sendContent)}, resourceWait);
          }
        }
      });

      //open a url to prerender
      page.open(url).then(function(status) {
        console.log('page opened', status);
        if (status !== 'success') {
          res.send('page could not be opened');
          page.close();
          // ph.exit();
          ph.kill();
          return;
        }

        //send the pre-renderred response after maxRenderWait of milliseconds if it wasn't sent before
        forcedRenderTimeout = setTimeout(function () {
          renderAndSend(page, ph, res, file_path, sendContent);
        }, maxRenderWait);

      }).catch(error => {
          console.log(error);
          try {
            // phInstance.exit();
            phInstance.kill();
          } catch (e) {}
      });

    }).catch(error => {
        console.log(error);
        try {
          // phInstance.exit();
          phInstance.kill();
        } catch (e) {}
    });
  }).catch(error => {
      console.log(error);
      try {
        // phInstance.exit();
        phInstance.kill();
      } catch (e) {}
  });
}

// expects a URL from root "/some/page" which will result in "protocol://host:port/some/page"
function absoluteUrl(req, relativeUrl) {
  var _origin = req.protocol + '://' + req.get('Host');
  relativeUrl = relativeUrl.charAt(0) !== "/" ? "/" + relativeUrl : relativeUrl;
  return _origin + relativeUrl
};

//send the html result of the prerenderred content to the client and stop phantom
function renderAndSend(page, ph, res, file_path, sendContent) {

  page.property('content').then(function (content) {
    if (sendContent) {
      res.send(content);
    }
    page.close();
    // ph.exit();
    ph.kill();
    clearTimeout(forcedRenderTimeout);
    clearTimeout(renderTimeout);
    cacheUrl(file_path, content);
    console.log('end time', Date());
  }).catch(error => {
    try {
      // phInstance.exit();
      phInstance.kill();
    } catch(e) {}
      console.log("could not send file content", error);
  });
}

function cacheUrl(file_path, content) {
  mkdirp.sync(path.dirname(file_path));
  fs.writeFile(file_path, content, function(err) {
      if(err) {
          return console.log('could not write in file', err);
      }

      console.log("The file was saved!");
  });
}

function parseUrl (url) {
  var parsed_url = URL.parse(url, true);

  parsed_url.search = null;
  if (typeof parsed_url.query._escaped_fragment_ != 'undefined') {
    delete parsed_url.query._escaped_fragment_;
  }
  return parsed_url.format(parsed_url);
}

var port = process.env.PORT || 8000;
app.listen(port, '0.0.0.0');
console.log("Listening on port " + port);
