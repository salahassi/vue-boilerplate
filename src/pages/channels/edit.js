import _ from 'lodash'
import layout from 'layouts/default'
import {tabs, tab} from 'components/tabs'
import { locationsTab } from './partials'

const components = {
  layout,
  tabs,
  tab,
  locationsTab
}

const computed = {
  channel () {
    return _.clone(this.$store.state.channels.active, true);
  }
}

const data = {

}

export default {
  name: 'edit-channel',
  template: require('./edit.html'),
  components,
  computed,
  data () {
    return data
  },
  created () {
    this.$store.dispatch('channels.get', this.$route.params.id);
  }
}
