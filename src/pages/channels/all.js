import layout from 'layouts/default'

const components = {
  layout
}

const computed = {
  channels () {
    return this.$store.state.channels.all
  }
}

export default {
  name: 'all-channels',
  template: require('./all.html'),
  components,
  computed,
  created () {
    this.$store.dispatch('channels.getAll');
  }
}
