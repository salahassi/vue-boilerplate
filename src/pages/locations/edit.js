import _ from 'lodash'
import layout from 'layouts/default'
import { tabs, tab } from 'components/tabs'
import { general, types, channels } from './partials'


const components = {
  layout,
  tabs,
  tab,
  types,
  channels,
  general
}

const methods = {
  onSubmitFom () {
    console.log(this.formData);
    this.errors = {}
    this.submit_loading = true;
    this.$store.dispatch('locations.edit', {id: this.$route.params.id, data: this.formData}).then(() => {
      this.submit_loading = false;
      if (this.$store.state.locations.errors) {
        this.errors = _.clone(this.$store.state.locations.errors, true);
        console.log(this.errors);
      }
    })
  },
  getLocation () {
    this.loading = true;
    this.$store.dispatch('locations.get', this.$route.params.id).then(() => {
      this.loading = false;
    });
  }
}

const computed = {
  formData () {
    return _.clone(this.$store.state.locations.active, true);
  }
}

const data = {
  loading: false,
  submit_loading: false,
  errors: {}
}

export default {
  name: 'edit-location',
  template: require('./edit.html'),
  components,
  methods,
  computed,
  data () {
    return data
  },
  created () {
    this.getLocation();
  }
}
