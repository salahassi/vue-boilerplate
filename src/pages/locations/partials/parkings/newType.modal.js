import _ from 'lodash'
import { baseModal } from 'components/modals'
import sSelect from 'components/sSelect'

const components = {
  sSelect,
  baseModal
}

const methods = {
  submit (modal) {
    let errors = false;
    this.errors = {};
    if (!this.form.selected) {
      this.errors.selected = "this field is required";
      errors = true;
    }
    if (!this.form.parkingsNumber) {
      this.errors.parkingsNumber = "this field is required";
      errors = true;
    }

    //validating totals
    let added_parkings = 0;
    for (let i in this.parkingTypes) {
      added_parkings += parseInt(this.parkingTypes[i].parkingsNumber);
    }

    console.log("added_parkings", added_parkings);
    console.log("added_parkings + this.form.parkingsNumber", added_parkings + parseInt(this.form.parkingsNumber));
    if (added_parkings + parseInt(this.form.parkingsNumber) > this.totalParkingsNumber) {
      this.errors.parkingsNumber = "Number of total parkings types must be less than or equal to " + this.totalParkingsNumber;
      this.errors.parkingsNumber += ", " + parseInt(this.totalParkingsNumber - added_parkings) + ' left.';
      errors = true;
    }

    if (errors) return;

    this.form.id = this.$route.params.id;
    let data = _.clone(this.form, true)
    this.$store.dispatch('locationTypes.newParkings', data);
    modal.close();
    this.form.id = null;
    this.form.parkingsNumber = null;
    this.form.selected = this.abstract[0];
    this.form.forRooms = false;
  }
}

const computed = {
  abstract () {
    let options = [];
    this.$store.getters.parkingsAbstract.forEach((item) => {
      options.push({
        value: item.id,
        text: item.name
      });
    });
    this.form.selected = options[0];
    return options
  },
  parkingTypes () {
    return this.$store.state.locationTypes.currentParkings;
  },
  totalParkingsNumber () {
    return this.$store.state.locations.active.parkingsNumber;
  }
}

export default {
  template: require('./newType.modal.html'),
  data () {
    return {
      form: {
        selected: {},
        parkingsNumber: null,
        forRooms: false
      },
      errors: {}
    }
  },
  methods,
  components,
  computed
}
