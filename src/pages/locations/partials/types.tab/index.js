import { baseModal } from 'components/modals'
import newRoomsType from '../rooms/newType.modal'
import newParkingsType from '../parkings/newType.modal'

const components = {
  baseModal,
  newRoomsType,
  newParkingsType
}

const computed = {
  roomTypes () {
    return this.$store.state.locationTypes.currentRooms;
  },
  parkingTypes () {
    return this.$store.state.locationTypes.currentParkings;
  }
}

const methods = {

}

const data = {

}

export default {
  name: "types",
  template: require('./types.tab.html'),
  props: ['formData', 'errors'],
  data () {
    return data
  },
  created () {
    this.$store.dispatch('locationTypes.getAbstract');
    this.$store.dispatch('locationTypes.getRoomTypes', this.$route.params.id);
    this.$store.dispatch('locationTypes.getParkingTypes', this.$route.params.id);
  },
  computed,
  methods,
  components
}
