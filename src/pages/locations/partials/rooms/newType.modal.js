import _ from 'lodash'
import { baseModal } from 'components/modals'
import sSelect from 'components/sSelect'

const components = {
  sSelect,
  baseModal
}

const methods = {
  submit (modal) {
    let errors = false;
    this.errors = {};
    if (!this.form.selected) {
      this.errors.selected = "this field is required";
      errors = true;
    }
    if (!this.form.roomsNumber) {
      this.errors.roomsNumber = "this field is required";
      errors = true;
    }

    //validating totals
    let added_rooms = 0;
    for (let i in this.roomTypes) {
      added_rooms += parseInt(this.roomTypes[i].roomsNumber);
    }

    console.log("added_rooms", added_rooms);
    console.log("added_rooms + this.form.roomsNumber", added_rooms + parseInt(this.form.roomsNumber));
    if (added_rooms + parseInt(this.form.roomsNumber) > this.totalRoomsNumber) {
      this.errors.roomsNumber = "Number of total rooms types must be less than or equal to " + this.totalRoomsNumber;
      this.errors.roomsNumber += ", " + parseInt(this.totalRoomsNumber - added_rooms) + ' left.';
      errors = true;
    }

    if (errors) return;
    this.form.id = this.$route.params.id;
    let data = _.clone(this.form, true)
    this.$store.dispatch('locationTypes.newRooms', data);
    modal.close();
    this.form.id = null;
    this.form.roomsNumber = null;
    this.form.selected = this.abstract[0];
  }
}

const computed = {
  abstract () {
    let options = [];
    this.$store.getters.roomsAbstract.forEach((item) => {
      options.push({
        value: item.id,
        text: item.name
      });
    });
    this.form.selected = options[0];
    return options
  },
  roomTypes () {
    return this.$store.state.locationTypes.currentRooms;
  },
  totalRoomsNumber () {
    return this.$store.state.locations.active.roomsNumber;
  }
}

export default {
  name: 'new-rooms-type',
  template: require('./newType.modal.html'),
  data () {
    return {
      form: {
        selected: {},
        roomsNumber: null
      },
      errors: {}
    }
  },
  methods,
  components,
  computed
}
