import parkingRateModal from '../parkingRateModal'
import roomRateModal from '../roomRateModal'

const components = {
  parkingRateModal,
  roomRateModal
}

const computed = {
  location () {
    return this.$store.state.locations.active
  }
}

const methods = {
  activateViewRates (channel) {
    console.log("channel", channel);
    this.activeChannel = channel;
  }
}

const data = {
  activeChannel: null
}

export default {
  name: 'chennels-tab',
  template: require('./channels.tab.html'),
  components,
  computed,
  methods,
  data () {
    return data
  }
}
