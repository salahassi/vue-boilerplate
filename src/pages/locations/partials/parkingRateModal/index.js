import _ from 'lodash'
import moment from 'moment'
import sSelect from 'components/sSelect'
import datepicker from 'components/datepicker'
import autocomplete from 'components/autocomplete'
import { baseModal } from 'components/modals'

const components = {
  baseModal,
  sSelect,
  datepicker,
  autocomplete
}

const data = {
  errors: {},
  form: {
    type: {value: null, text: '-- Choose --'},
    parkingsNumber: null,
    daily_or_hourly: 'daily',
    forRooms: false,
    fromDate: null,
    price: null,
    toDate: null
  },
  selectedChannels: []
}

const computed = {
  types () {
    let types = this.$store.state.locationTypes.currentParkings.map((item) => {
      return _.merge({text: item.selected.text, value: item.id}, item)
    });
    if (types.length) {
      this.form.type = types[0];
    }
    return types;
  },
  channels () {
    return _.clone(this.$store.state.locations.active.channels, true);
  },
  totalAvailable () {
    let blanks = 0;// number of rates that don't have expiration date
    let expires = 0;// number of rates that have expiration date greater than startAt
    let visited_rates = [];//this should be got rid of when using data from the server
    this.channels.forEach((channel) => {
      channel.parkingRates.forEach((rate) => {
        console.log('this.form.type.id', this.form.type.id);
        console.log('rate.type', rate.type);
        console.log('this.form.type.id != rate.type.id', this.form.type.id != rate.type.id);
        if (this.form.type.id != rate.type.id) return;//this will be right when using real data
        if (visited_rates.indexOf(rate) != -1) {
          console.log('returned',visited_rates);
          return;
        }

        visited_rates.push(rate);
        if (this.form.endDate) {
          //if the current rate doesnt have end date and
          //end date of new rate is greater than start date of current date
          if(!rate.toDate &&
            (moment(this.form.endDate, ['MM/DD/YYYY']) > moment(rate.fromDate, ['MM/DD/YYYY']))) {
            blanks += parseInt(rate.parkingsNumber);
          } else if (//if the current rate has end date and it's greater than new rates' start date and
                     //end date of new rate is greater than start date of current date
            moment(rate.toDate, ['MM/DD/YYYY']) > moment(this.form.fromDate, ['MM/DD/YYYY']) &&
            (moment(this.form.endDate, ['MM/DD/YYYY']) > moment(rate.fromDate, ['MM/DD/YYYY']))
          ) {
            expires += parseInt(rate.parkingsNumber);
          }
        } else {// if the new rate doesnt have end date
          if(!rate.toDate) {
            blanks += parseInt(rate.parkingsNumber);
          } else if (moment(rate.toDate, ['MM/DD/YYYY']) > moment(this.form.fromDate, ['MM/DD/YYYY'])) {
            expires += parseInt(rate.parkingsNumber);
          }
        }
      })
    })
    // console.log('this.form.type.parkingsNumber', this.form.type.parkingsNumber);
    // console.log('blanks', blanks);
    // console.log('expires', expires);
    return parseInt(this.form.type.parkingsNumber) - blanks - expires;
  }
}

const methods = {
  submit (modal) {
    this.errors = {}
    let required = {
      type: "" + this.form.type.value,
      parkingsNumber: this.form.parkingsNumber,
      fromDate: this.form.fromDate,
      price: this.form.price
    }

    let hasError = false;

    for (let i in required) {
      if (!required[i]) {
        this.errors[i] = 'this field is required';
        hasError = true;
      }
    }

    if (!this.selectedChannels.length) {
      this.errors.channels = 'You must select at least one channel';
      hasError = true;
    }

    if (this.form.parkingsNumber > this.totalAvailable) {
      this.errors.parkingsNumber = 'Parkings number must not exceed the total remaining number';
      hasError = true;
    }

    if (this.form.parkingsNumber < 1) {
      this.errors.parkingsNumber = 'Parkings number must be a valid number';
      hasError = true;
    }

    if (hasError) {
      return;
    }

    let data = _.clone(this.form, true);

    for (let i in this.selectedChannels) {
      this.$store.dispatch('locations.addPriceToChannel', {id: this.selectedChannels[i].id, type: 'parking', data});
    }

    modal.close();
    this.form = {
      type: this.types[0],
      parkingsNumber: null,
      daily_or_hourly: 'daily',
      forRooms: false,
      fromDate: null,
      price: null,
      toDate: null
    }
    this.selectedChannels = [];
  }
}

export default {
  name: "parking-rate-modal",
  template: require('./parkingRateModal.html'),
  components,
  computed,
  methods,
  data () {
    return data
  }
}
