export {default as types} from "./types.tab"
export {default as channels} from "./channels.tab"
export {default as general} from "./general.tab"
export {default as newRoomsType} from "./rooms/newType.modal"
export {default as parkingRateModal} from './parkingRateModal'
export {default as roomRateModal} from './roomRateModal'
