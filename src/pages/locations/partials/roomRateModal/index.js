import _ from 'lodash'
import moment from 'moment'
import sSelect from 'components/sSelect'
import datepicker from 'components/datepicker'
import autocomplete from 'components/autocomplete'
import { baseModal } from 'components/modals'

const components = {
  baseModal,
  sSelect,
  datepicker,
  autocomplete
}

const data = {
  errors: {},
  form: {
    type: {value: null, text: '-- Choose --'},
    roomsNumber: null,
    fromDate: null,
    price: null,
    toDate: null
  },
  selectedChannels: []
}

const computed = {
  types () {
    let types = this.$store.state.locationTypes.currentRooms.map((item) => {
      return _.merge({text: item.selected.text, value: item.id}, item)
    });
    if (types.length) {
      this.form.type = types[0];
    }
    return types;
  },
  channels () {
    return _.concat([], this.$store.state.locations.active.channels);
  },
  totalAvailable () {
    let blanks = 0;// number of rates that don't have expiration date
    let expires = 0;// number of rates that have expiration date greater than startAt
    let visited_rates = [];//this should be got rid of when using data from the server
    this.channels.forEach((channel) => {
      channel.roomRates.forEach((rate) => {
        if (JSON.stringify(visited_rates).indexOf(JSON.stringify(rate)) != -1) {
          return;
        }

        visited_rates.push(rate);
        if (this.form.endDate) {
          //if the current rate doesnt have end date and
          //end date of new rate is greater than start date of current date
          if(!rate.toDate &&
            (moment(this.form.endDate, ['MM/DD/YYYY']) > moment(rate.fromDate, ['MM/DD/YYYY']))) {
            blanks += parseInt(rate.roomsNumber);
          } else if (//if the current rate has end date and it's greater than new rates' start date and
                     //end date of new rate is greater than start date of current date
            moment(rate.toDate, ['MM/DD/YYYY']) > moment(this.form.fromDate, ['MM/DD/YYYY']) &&
            (moment(this.form.endDate, ['MM/DD/YYYY']) > moment(rate.fromDate, ['MM/DD/YYYY']))
          ) {
            expires += parseInt(rate.roomsNumber);
          }
        } else {// if the new rate doesnt have end date
          if(!rate.toDate) {
            blanks += parseInt(rate.roomsNumber);
          } else if (moment(rate.toDate, ['MM/DD/YYYY']) > moment(this.form.fromDate, ['MM/DD/YYYY'])) {
            expires += parseInt(rate.roomsNumber);
          }
        }
      })
    })
    // console.log('this.form.type.roomsNumber', this.form.type.roomsNumber);
    // console.log('blanks', blanks);
    // console.log('expires', expires);
    return parseInt(this.form.type.roomsNumber) - blanks - expires;
  }
}

const methods = {
  autocompleteSelect (event, ui) {
    event.target.value = "";
    if (JSON.stringify(this.selectedChannels).indexOf(JSON.stringify(ui.item)) == -1) {
      this.selectedChannels.push(ui.item);
    }
  },
  autocompleteFormat (ul, item) {
    // console.log(item);
    return $( "<li>" )
      .append( "<div>" + item.name + "<br/><b>" + item.domain + "</b></div>" )
      .appendTo( ul );
  },
  removeChannel (i) {
    this.selectedChannels.splice(i, 1);
  },
  submit (modal) {
    this.errors = {}
    let required = {
      type: "" + this.form.type.value,
      roomsNumber: this.form.roomsNumber,
      fromDate: this.form.fromDate,
      price: this.form.price
    }

    let hasError = false;

    for (let i in required) {
      if (!required[i]) {
        this.errors[i] = 'this field is required';
        hasError = true;
      }
    }

    if (!this.selectedChannels.length) {
      this.errors.channels = 'You must select at least one channel';
      hasError = true;
    }

    if (this.form.roomsNumber > this.totalAvailable) {
      this.errors.roomsNumber = 'Rooms number must not exceed the total remaining number';
      hasError = true;
    }

    if (this.form.roomsNumber < 1) {
      this.errors.roomsNumber = 'Rooms number must be a valid number';
      hasError = true;
    }

    if (hasError) {
      return;
    }

    let data = _.clone(this.form, true);

    for (let i in this.selectedChannels) {
      this.$store.dispatch('locations.addPriceToChannel', {id: this.selectedChannels[i].id, type: 'room', data});
    }

    modal.close();
    this.form = {
      type: this.types[0],
      roomsNumber: null,
      fromDate: null,
      price: null,
      toDate: null
    }
    this.selectedChannels = [];
    this.errors = {};
  }
}

export default {
  name: "room-rate-modal",
  template: require('./roomRateModal.html'),
  components,
  computed,
  methods,
  data () {
    return data
  }
}
