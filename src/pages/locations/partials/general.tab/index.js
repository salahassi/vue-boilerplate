export default {
  name: 'general-tab',
  template: require('./general.tab.html'),
  props: ['formData', 'errors']
}
