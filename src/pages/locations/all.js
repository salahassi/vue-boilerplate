import _ from 'lodash'
import layout from 'layouts/default'
import pagination from 'components/pagination'

import './all.scss'

const components = {
  layout,
  pagination
}

const methods = {
  loadPage (page) {
    this.loading = true;
    this.$store.dispatch('locations.getAll', {page, _q: this.searchKey}).then (() => {
      this.loading = false;
    });
  },
  search (searchKey) {
    this.searchKey = searchKey;
    this.loadPage(1);
  }
}

const computed = {
  locations () {
    return _.clone(this.$store.state.locations.all, true)
  }
}

const data = {
  loading: true,
  searchKey: null
}

export default {
  template: require('./all.html'),
  name: 'Main',
  components,
  methods,
  computed,
  data () {
    return data
  },
  created () {
    // this.$store.dispatch('channels.getAll');
    this.loadPage (this.$route.query.page || 1);
  },
  mounted () {

  }
}
