// import Main from 'pages/main/main'
import PageNotFound from 'pages/notfound/pageNotFound'
import {all as allLocations, edit as editLocation} from 'pages/locations'
import {all as allChannels, edit as editChannel} from 'pages/channels'

const routes = [
  { path: '/locations', name: 'allLocations', component: allLocations,
    meta: permits: ['location_list']},
  { path: '/locations/:id', name: 'editLocation', component: editLocation},
  { path: '/channels', name: 'allChannels', component: allChannels},
  { path: '/channels/:id', name: 'editChannel', component: editChannel},
  { path: '/', name: 'home', redirect: {name: 'allLocations'} },
  { path: '*', name: 'notfound', component: PageNotFound }
]

export default routes
