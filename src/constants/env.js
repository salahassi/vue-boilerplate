const process_env = ['development', 'dev_server'].indexOf(process.env.NODE_ENV) > -1
  ? 'dev'
  : process.env.NODE_ENV;

export default require('./' + process_env + '.env');
