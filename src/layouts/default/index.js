import hasPermits from 'directives/hasPermits'

import "./nav.scss"

const directives = {
  hasPermits
}

const methods = {
  changeRoute (route) {
    this.$store.dispatch('nav.changeRoute', route);
  },
  logout () {
    this.$store.dispatch('user.logout');
    this.$router.replace({name: 'login'});
  }
}

export default {
  template: require ('./default.html'),
  methods,
  directives,
  created () {
    this.changeRoute(this.$route.name);
  }
}
