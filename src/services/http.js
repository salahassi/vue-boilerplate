import axios from 'axios'
import env from 'constants/env'
import router from './router'
import user from './user'

let promise;
let instance = axios.create({
  baseURL: env.default.api
});

//Handle auth
if (!user.getToken()) {
  router.replace({
    name: 'login'
  });
}

instance.interceptors.request.use((config) => {
  if (user.getToken()) {
    config.headers.common['Authorization'] = "Bearer " + user.getToken();
  }
  return config;
});

instance.interceptors.response.use((response) => {
  // Do something with response data
  console.log('response', response);
  return response;
}, function (error) {
  // Do something with response error
  console.log('error.response.status', error.response.status);
  if ([401].indexOf(error.response.status) > -1) {
    console.log(error.response);
    if(error.response.data.error !== 'invalid_credentials') {
      refreshToken(error.response.config);
      return new Promise(error);
    }
  } else if ([403].indexOf(error.response.status) > -1) {
    router.replace({
      name: 'home'
    });
  }
  return Promise.reject(error);
});

function refreshToken (config) {
  instance.post('users/auth/refresh-token').then((res) => {
    console.log('res.data.newToken', res.data.newToken);
    user.setToken(res.data.newToken);
    reRequest(config);
  })
  .catch((error) => {
    router.replace({
      name: 'login'
    });
    return Promise.reject(error);
  })
}

function reRequest (config) {
  config.headers.Authorization = "Bearer " + user.getToken();
  axios(config)
  .then((res) => {
    router.go({path: router.currentRoute.fullpath});
    Promise.resolve(res);
  })
  .catch((error) => {
    Promise.reject(error);
  });
}

export default instance
