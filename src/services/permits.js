import _ from 'lodash'
import user from './user'

class Permits {
  constructor () {
    this.user = user.getUser();
  }

  hasPermit(permit) {
    if (!this.user) return false;
    if (this.user.permissions.indexOf('super_admin') > -1)
      return true;

    return this.user.permissions.indexOf(permit) > -1;
  }

  //check if the user has at least one permission from the list
  hasPermits(arr = []) {
    for (let i in arr) {
      if (this.hasPermit(arr[i])) {
        return true;
      }
    }

    return false;
  }
}

export default new Permits();
