import Vue from 'vue'
import VueRouter from 'vue-router'
import routes from 'routes'
import { permits } from 'services'

Vue.use(VueRouter)

//vue router options
let router = new VueRouter({
  hashbang: true,
  mode: 'history',
  routes, // short for routes: routes
})

router.beforeEach((to, from, next) => {
  if (to.meta.permits) {
    if (permits.hasPermits(to.meta.permits)) {
      next();
    } else {
      next(false);
    }
  } else {
    next();
  }
});

export default router;
