import 'es6-promise/auto'

import Vue from 'vue'
import { router } from 'services'
import store from 'store'

import 'sass/master.scss'

//mount vue to #app
let app = new Vue({
  store,
  router
}).$mount('#app')
