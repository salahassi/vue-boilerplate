import Vue from 'vue'
import Vuex from 'vuex'
import * as actions from './actions'
import * as getters from './getters'
import * as mutations from './mutations'
import nav from './modules/nav'
import locations from './modules/locations'
import locationTypes from './modules/locationTypes'
import channels from './modules/channels'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'prod'

export default new Vuex.Store({
  actions,
  getters,
  mutations,
  modules: {
    nav,
    locations,
    locationTypes,
    channels
  },
  strict: debug,
})
