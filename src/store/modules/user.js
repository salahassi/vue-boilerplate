import { http, user } from 'services'
// initial state
const state = {
  token: user.getToken(),
  data: user.getUser(),
  errors: null
}

// getters
const getters = {
}

// actions
const actions = {
  ['user.login'] ({ commit }, {email, password}) {
    commit('CLEAR_ERRORS');
    return http.post(`users/auth`, {email, password})
    .then((res) => {
      commit('USER_LOGIN', res);
    })
    .catch((err) => {
      commit('USER_ERROR', err);
    });
  },
  ['user.logout'] ({ commit }) {
    commit('CLEAR_ERRORS');
    commit('USER_LOGOUT');
  },
  ['user.signup'] ({ commit }, data) {
    commit('CLEAR_ERRORS');
  },
  ['user.updatePersonalData'] ({ commit }, data) {
    commit('CLEAR_ERRORS');
    //TODO
    return http.put(``, data)
    .then((res) => {
      commit('USER_UPDATED', res);
    })
    .catch((err) => {
      commit('USER_ERROR', err);
    });;
  }
}

// mutations
const mutations = {
  USER_LOGIN (state, res) {
    state.token = res.data.token;
    state.data = res.data.data;
    user.setUser(state.data);
    user.setToken(state.token);
  },
  USER_UPDATED (state, res) {
    state.data = res.data.data;
    user.setUser(state.data);
  },
  USER_LOGOUT (state) {
    state.token = null;
    state.data = null;
    user.setUser(state.data);
    user.setToken(state.token);
  },
  //ERRORS
  USER_ERROR (state, err) {
    state.errors = err.response.data;
    user.setUser(null);
    user.setToken(null);
  },

  CLEAR_ERRORS (state) {
    state.errors = null
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
