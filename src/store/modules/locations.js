import { http } from 'services'

let locations = [];
for (let i = 0; i < 50; i++) {
  locations.push({
    name: ["dummy text of the printing", "Lorem Ipsum", 'typesetting industry'][i % 3] + Math.round(Math.random() * 10000000),
    airport: ["dummy text of the printing", "Lorem Ipsum", 'typesetting industry'][i % 3] + Math.round(Math.random() * 10000000),
    address: ["dummy text of the printing", "Lorem Ipsum", 'typesetting industry'][i % 3],
    phone: "(000) 000 0000",
    roomsNumber: 50 + Math.round(Math.random() * 150),
    parkingsNumber: 50 + Math.round(Math.random() * 150),
    channels: [],
    id: i
  });
}

// initial state
const state = {
  all: [],
  active: {},
  errors: null
}

// getters
const getters = {

}

// actions
const actions = {
  //get all locations (params: {_q: search name, page})
  ['locations.getAll'] ({ commit, state, rootState }, params) {
    if (!params._q) params._q = null

    return (http.get('locations', {
      params
    })).then ((res) => {
      commit('GET_ALL_LOCATIONS', res);
    }).catch ((res) => {
      commit('LOCATIONS_ERROR', res);
    })
  },
  //get single location by id
  ['locations.get'] ({commit, state}, id) {
    return (http.get(`locations/${id}`, {
      params: {
        _with: 'channels'
      }
    })).then ((res) => {
      commit('GET_LOCATION', res);
    }).catch ((res) => {
      commit('LOCATIONS_ERROR', res);
    })
  },
  //update location
  ['locations.edit'] ({commit, state}, {id, data}) {
    return (http.put(`locations/${id}`, data)).then ((res) => {
      commit('EDIT_LOCATION', res);
    }).catch ((res) => {
      commit('LOCATIONS_ERROR', res);
    })
  },

  ['locations.addPriceToChannel'] ({commit}, {id, type, data}) {
    commit('ADD_PRICE_TO_CHANNEL', {id, type, data});
  },

  ['locations.remove'] (id) {

  },

  ['locations.update'] (id, data) {

  },
  ['locations.add'] (data) {

  }
}

// mutations
const mutations = {
  GET_ALL_LOCATIONS (state, res) {
    state.errors = null;
    state.all = res.data;
  },
  GET_LOCATION (state, active) {
    state.errors = null;
    state.active = active.data;
  },
  EDIT_LOCATION (state, res) {
    state.errors = null;
    state.active = res.data;
  },
  ADD_PRICE_TO_CHANNEL (state, {id, type, data}) {
    state.errors = null;
    state.active.channels[id][type + 'Rates'].push(data);
  },
  //ERRORS
  LOCATIONS_ERROR (state, err) {
    console.log('LOCATIONS_ERROR', err.response.data);
    state.errors = err.response.data;
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
