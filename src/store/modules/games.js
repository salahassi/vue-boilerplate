import axios from 'axios'
import env from 'constants/env'

// initial state
const state = {
  all: []
}

// getters
const getters = {
}

// actions
const actions = {
  getGames ({ commit }) {
    return null;
    // return (axios.get(env.default.api + 'predictor')).then((res) => {
    //   commit('GET_GAMES', res.data);
    // });
  }
}

// mutations
const mutations = {
  GET_GAMES (state, games) {
    state.all = games;
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
