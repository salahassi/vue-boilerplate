// initial state
const state = {
  message: 'hello'
}

// getters
const getters = {
  reversedMessage: state => state.message.reverse()
}

// actions
const actions = {
  updateMessage ({ commit }, text) {
    commit('UPDATE_MESSAGE', text );
  }
}

// mutations
const mutations = {
  UPDATE_MESSAGE (state, text) {
    console.log('dispatched', state, text);
    state.message = text;
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
