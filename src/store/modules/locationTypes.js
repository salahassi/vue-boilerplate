let abstract = [];
let names = ['Lorem Ipsum', 'is simply dummy', 'text of the printing', 'typesetting industry'];
for (let i = 0; i < 20; i++) {
  abstract.push({
    id: i,
    name: names[i % 4],
    type: i % 3 == 0 ? 'room' : 'parking'
  });
}

let rooms = [];
let parkings = [];

// initial state
const state = {
  abstract: [],
  rooms: [],
  parkings: [],
  currentRooms: [],
  currentParkings: [],
  activeRoom: {},
  activeParking: {}
}

// getters
const getters = {
  roomsAbstract: state => state.abstract.filter(item => item.type == 'room'),
  parkingsAbstract: state => state.abstract.filter(item => item.type == 'parking')
}

// actions
const actions = {
  ['locationTypes.getAbstract'] ({ commit, state }) {
    if (!state.abstract.length)
      commit('GET_ALL_ABSTRACT');
  },
  ['locationTypes.rooms'] ({commit, state}) {
    if (!state.rooms.length)
      commit('GET_ALL_ROOMS');
  },
  ['locationTypes.parkings'] ({commit, state}) {
    if (!state.parkings.length)
      commit('GET_ALL_PARKINGS');
  },
  ['locationTypes.newRooms'] ({commit}, data) {
    commit('ADD_NEW_ROOMS_TYPE', data);
    commit('GET_ROOM_TYPES', data.id);
  },
  ['locationTypes.newParkings'] ({commit}, data) {
    commit('ADD_NEW_PARKINGS_TYPE', data);
    commit('GET_PARKING_TYPES', data.id);
  },
  ['locationTypes.getRoomTypes'] ({commit}, location_id) {
    commit('GET_ROOM_TYPES', location_id);
  },
  ['locationTypes.getParkingTypes'] ({commit}, location_id) {
    commit('GET_PARKING_TYPES', location_id);
  }
}

// mutations
const mutations = {
  GET_ALL_ABSTRACT (state) {
    state.abstract = abstract;
  },
  GET_ALL_ROOMS (state) {
    state.rooms = rooms;
  },
  GET_ALL_PARKINGS (state) {
    state.parkings = parkings;
  },
  ADD_NEW_ROOMS_TYPE (state, data) {
    state.rooms.push(data);
  },
  ADD_NEW_PARKINGS_TYPE (state, data) {
    state.parkings.push(data);
  },
  GET_ROOM_TYPES (state, id) {
    state.currentRooms = [];
    state.rooms.forEach(item => {
      if (item.id == id) {
        state.currentRooms.push(item);
      }
    });
  },
  GET_PARKING_TYPES (state, id) {
    state.currentParkings = [];
    state.parkings.forEach(item => {
      if (item.id == id) {
        state.currentParkings.push(item);
      }
    });
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
