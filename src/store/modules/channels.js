let channels = [];
for (let i = 0; i < 15; i++) {
  channels.push({
    name: ["dummy text of the printing", "Lorem Ipsum", 'typesetting industry'][i % 3] + Math.round(Math.random() * 10000000),
    domain: ['http://www.onestopparking.com', 'http://www.parkflyfresh.com', 'http://www.parksleepfly.com'][i % 3],
    phone: "(000) 000 0000",
    id: i,
    roomRates: [],
    parkingRates: []
  });
}

// initial state
const state = {
  all: [],
  active: {}
}

// getters
const getters = {

}

// actions
const actions = {
  ['channels.getAll'] ({ commit }) {
    if (!state.all.length) {
      commit('GET_ALL_CHANNELS');
    }
  },
  ['channels.get'] ({commit, state}, id) {
    if (!state.all.length) {
      commit('GET_ALL_CHANNELS');
    }

    for (let i in state.all) {
      console.log(state.all[i].id == id);
      if (state.all[i].id == id) {
        commit('GET_CHANNEL', state.all[i]);
        break;
      }
    }
  },
  ['channels.remove'] (id) {

  },
  ['channels.update'] (id, data) {

  },
  ['channels.add'] (data) {

  }
}

// mutations
const mutations = {
  GET_ALL_CHANNELS (state) {
    state.all = channels;
  },
  GET_CHANNEL (state, active) {
    state.active = active;
  },
}

export default {
  state,
  getters,
  actions,
  mutations
}
