/*
* @INFO: this component renders bootstrap tabs along side with "tab" component
*/
const methods = {

}

export default {
  template: require('./tabs.html'),
  data () {
    return {
      titles: [],
      active: null
    }
  },
  mounted () {
    this.active = this.titles[0]
  },
  methods
}
