const computed = {
  active () {
    return this.$parent.active == this.title
  }
}

export default {
  template: require('./tab.html'),
  props: ['title'],
  created () {
    this.$parent.titles.push(this.title);
  },
  computed
}
