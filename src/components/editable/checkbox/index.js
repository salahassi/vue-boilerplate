/*
* @DESC: this component makes an element editable checkbox when double click on it.
* @param value: the value that you need to change
* @param autoCloseOnSave: true to close the component without loading
* @param trueText: the text that shows up when the checkbox is checked
* @param falseText: the text that shows up when the checkbox is not checked
* @callback: v-on:valueSaved="doSomething(data: {value, close(), startLoading(), startLoading()})"
*/

import _ from 'lodash'

import './checkbox.scss'

const methods = {
  cancel () {
    this.editedValue = this.cachedVal;
    this.active = false;
  },
  close () {
    this.active = false;
  },
  startLoading () {
    this.loading = true;
  },
  stopLoading () {
    this.loading = false;
  },
  save () {
    this.editedValue = $(this.$refs.input).is(':checked');
    this.$emit('valueSaved', {
      value: this.editedValue,
      close: this.close,
      startLoading: this.startLoading,
      stopLoading: this.stopLoading
    });

    console.log(this.editedValue);
    if (this.autoCloseOnSave) {
      this.active = false;
      this.stopLoading();
    }
  },
  enterActiveMode () {
    this.active = true;
    console.log('double clicked');
  }
}

const computed = {

}

const watch = {
  active (n) {
    if (n) {
      setTimeout(() => {
        $(this.$refs.input).focus();
      }, 100)
      this.cachedVal = _.cloneDeep(this.value);
    }
    console.log(this.cachedVal);
  },
  value (n, o) {
    if(n == o) return;
    this.editedValue = _.cloneDeep(n);
  }
}

export default {
  name: 'editable-checkbox',
  template: require('./checkbox.html'),
  props: ['value', 'autoCloseOnSave', 'falseText', 'trueText'],
  methods,
  computed,
  watch,
  data () {
    return {
      active: false,
      cachedVal: null,
      loading: false,
      editedValue: _.cloneDeep(this.value)
    }
  },
  mounted () {

  }
}
