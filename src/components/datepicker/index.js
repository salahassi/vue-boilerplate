/*
* @INFO: This component makes a a jquery-ui datepicker
* @PROP value: (pass it as v-model)
* @PROP minDate: is the minimum date to reach
* @PROP maxDate: is the maximum date to reach
*/
import moment from 'moment'
import 'jquery-ui'

const methods = {
  reload () {
    $(this.$refs.input).datepicker("destroy");
    $(this.$refs.input).datepicker({
      minDate: this.minDate || moment().format('MM/DD/YYYY'),
      maxDate: this.maxDate,
      onClose: this.onChange
    }).css("z-index", "9999");
  },
  onChange (value) {
    this.$emit('input', value)
  }
}

const watch = {
  maxDate: function () {
    this.reload()
  },
  minDate: function () {
    this.reload()
  }
}

const computed = {
  // newValue () {
  //   return this.value
  // }
}

export default {
  name: 'datepicker',
  template: require('./datepicker.html'),
  props: ['maxDate', 'minDate', 'value'],
  methods,
  watch,
  computed,
  mounted () {
    this.reload();
  }
}
