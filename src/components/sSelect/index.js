/*
* @INFO: this component is a rebuild of "select" element
* @PROP selected: is the selected option -> {text: 'the text to show', value: 'the value of the option'}
* @PROP options: an array of options -> [{text: '...', value: '...'}]
* @CALLBACK selectedChanged: fires when the selected option has changed
*/
import './select.scss'
const computed = {
  value () {
    let fallback = {
      value: "",
      text: "-- wrong selected prop --"
    };
    try {
      return typeof this.selected.value == 'undefined' ? fallback : this.selected;
    } catch (e) {
      return typeof this.selected == 'undefined' ? fallback : this.selected;
    }
  }
}

const methods = {
  selectOption (option) {
    this.$emit('selectedChanged', option);
  }
}

export default {
  name: 's-select',
  template: require('./select.html'),
  props: {
    'selected': {
      required: true,
      type: Object
    },
    'options': {
      required: true,
      type: Array
    }
  },
  data () {
    return {
      // active: this.selected
    }
  },
  computed,
  methods
}
