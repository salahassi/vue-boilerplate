import moment from 'moment'
import axios from 'axios'

export default {
  name: 'testComponent',
  data () {
    return {
      now: moment().format('HH:mm:ss'),
      list: []
    }
  },
  created () {
    // alert('alert');
    window.setInterval(() => {
      this.now = moment().format('HH:mm:ss');
      console.log('mounted');
    }, 1000);
    // this.now = "shit";
    document.title = "the title has been changed";
    axios.get('https://jsonplaceholder.typicode.com/posts').then((res) => {
      document.title = "the title has been changed again";
      console.log(res);
      this.list = res.data;
    });
    axios.get('https://jsonplaceholder.typicode.com/comments').then((res) => {
      document.title = "the title has been changed again 2";
      console.log(res);
      this.list = res.data;
    });
    axios.get('https://jsonplaceholder.typicode.com/albums').then((res) => {
      document.title = "the title has been changed again 3";
      console.log(res);
      this.list = res.data;
    });
    axios.get('https://jsonplaceholder.typicode.com/photos').then((res) => {
      document.title = "the title has been changed again 4";
      console.log(res);
      this.list = res.data;
    });
  },
  computed: {
    timer () {

      return this.now;
    }
  },
  template: `
    <div>{{$store.state.test.message}}
      {{now}}
      <ul>
        <li v-for="item in list">{{item.title}}</li>
      </ul>
    </div>
  `
}
