import { permits } from 'services'

export default {
  name: "hasPermits",
  inserted (el, binding) {
    //if the user doesn't have any permission that listed in "allow", prevent his/her access
    if (binding.arg == 'allow') {
      if (!permits.hasPermits(binding.value)) {
        $(el).remove();
      }
    }

    //if one of these permissions exists, prevent his/her access
    if (binding.arg == 'prevent') {
      if (permits.hasPermits(binding.value)) {
        $(el).remove();
      }
    }
  }
}
